module m_constants

    integer, parameter::ROWS = 22
    integer, parameter::COLUMNS = 10
    
    integer, parameter::ROW_VALUE = 100

    integer, parameter::DOWN = 1
    integer, parameter::LEFT = 2
    integer, parameter::RIGHT = 3

    integer, parameter::SCREEN_W = 640
    integer, parameter::SCREEN_H = 480
    
    real, parameter::UPDATE_SECONDS = 0.4

end module m_constants
