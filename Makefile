#
# Automagically generated by Approximatrix Simply Fortran 2.15
#
FC="C:\Program Files (x86)\Simply Fortran 2\mingw-w64\bin\gfortran.exe"
CC="C:\Program Files (x86)\Simply Fortran 2\mingw-w64\bin\gcc.exe"
AR="C:\Program Files (x86)\Simply Fortran 2\mingw-w64\bin\ar.exe"
WRC="C:\PROGRA~2\SIMPLY~1\MINGW-~1\bin\windres.exe"
RM=rm -f

IDIR=-IC:/Users/Jeffrey/AppData/Local//sfpm/32/include 
# -I error: Directory C:\Program Files (x86)\Simply Fortran 2\mingw-w64\include\ does not exist

LDIR=-LC:/PROGRA~2/SIMPLY~1/MINGW-~1/lib/ -LC:/Users/Jeffrey/AppData/Local//sfpm/32/lib 


OPTFLAGS= -g

SPECIALFLAGS=-m32 $(IDIR)

RCFLAGS=-O coff -F pe-i386

PRJ_FFLAGS= -fcheck=all

PRJ_CFLAGS=

PRJ_LFLAGS=-lbgif -lbgi -lgdi32 -lcomdlg32 -luuid -loleaut32 -lole32 -lstdc++

FFLAGS=$(SPECIALFLAGS) $(OPTFLAGS) $(PRJ_FFLAGS) -Jmodules 

CFLAGS=$(SPECIALFLAGS) $(OPTFLAGS) $(PRJ_CFLAGS)

"build\constants.o": ".\constants.f90"
	@echo Compiling .\constants.f90
	@$(FC) -c -o "build\constants.o" $(FFLAGS) ".\constants.f90"
"modules\m_constants.mod" : "build\constants.o" .EXISTSONLY
	@echo Compiling .\constants.f90
	@$(FC) -c -o "build\constants.o" $(FFLAGS) ".\constants.f90"

"build\game.o": ".\game.f90" "modules\m_constants.mod" "modules\m_piece.mod"
	@echo Compiling .\game.f90
	@$(FC) -c -o "build\game.o" $(FFLAGS) ".\game.f90"
"modules\m_game.mod" : "build\game.o" .EXISTSONLY
	@echo Compiling .\game.f90
	@$(FC) -c -o "build\game.o" $(FFLAGS) ".\game.f90"

"build\main.o": ".\main.f90" "modules\m_game.mod" "modules\m_screen.mod" "modules\m_piece.mod" "modules\m_constants.mod"
	@echo Compiling .\main.f90
	@$(FC) -c -o "build\main.o" $(FFLAGS) ".\main.f90"

"build\piece.o": ".\piece.f90" "modules\m_constants.mod"
	@echo Compiling .\piece.f90
	@$(FC) -c -o "build\piece.o" $(FFLAGS) ".\piece.f90"
"modules\m_piece.mod" : "build\piece.o" .EXISTSONLY
	@echo Compiling .\piece.f90
	@$(FC) -c -o "build\piece.o" $(FFLAGS) ".\piece.f90"

"build\screen.o": ".\screen.f90" "modules\m_constants.mod" "modules\m_game.mod"
	@echo Compiling .\screen.f90
	@$(FC) -c -o "build\screen.o" $(FFLAGS) ".\screen.f90"
"modules\m_screen.mod" : "build\screen.o" .EXISTSONLY
	@echo Compiling .\screen.f90
	@$(FC) -c -o "build\screen.o" $(FFLAGS) ".\screen.f90"

clean: .SYMBOLIC
	@echo Deleting build\constants.o and related files
	@$(RM) "build\constants.o" "modules\m_constants.mod"
	@echo Deleting build\game.o and related files
	@$(RM) "build\game.o" "modules\m_game.mod"
	@echo Deleting build\main.o and related files
	@$(RM) "build\main.o"
	@echo Deleting build\piece.o and related files
	@$(RM) "build\piece.o" "modules\m_piece.mod"
	@echo Deleting build\screen.o and related files
	@$(RM) "build\screen.o" "modules\m_screen.mod"
	@echo Deleting fetris.exe
	@$(RM) "fetris.exe"

"fetris.exe":  "build\constants.o" "build\game.o" "build\main.o" "build\piece.o" "build\screen.o"
	@echo Generating fetris.exe
	@$(FC) -o "fetris.exe" -static -m32 "build\constants.o" "build\game.o" "build\main.o" "build\piece.o" "build\screen.o" $(LDIR) $(PRJ_LFLAGS)

all: "fetris.exe" .SYMBOLIC

