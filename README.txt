Fetris
======

A Tetris clone written in pure Fortran
by Jeff Armstrong

Introduction
------------

Fetris is a pretty simple Tetris clone that plays much like one would expect
from a Tetris-like game.  The usual pieces are present, and the controls are
working as expected.  The game isn't pretty, but it does work.

Graphics are provided by a Fortran wrapper around WinBGIm, a modern
implementation of the old Borland Graphics Interface for Microsoft Windows.

Building
--------

A Simply Fortran (http://simplyfortran.com) project is provided with the
source code.

Playing
-------

After starting the game, play is reasonably straightforward.  The keys are:

LEFT and RIGHT      Move current piece left or right
UP                  Rotate piece 90 degrees clockwise
DOWN                Move piece down faster
Q                   Quits

Copyright
---------

Please consider Fetris in the public domain.
