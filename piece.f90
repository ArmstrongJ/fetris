module m_piece
implicit none

    type piece
        integer, dimension(0:3, 0:3)::blocks
        integer::r, c
    end type piece

contains

    function new_piece()
    use m_constants
    use bgi
    implicit none
    
    type(piece)::new_piece
    real::r
    integer::rint
        
        new_piece%r = ROWS-1
        new_piece%c = COLUMNS/2-2

        call random_number(r)
        rint = floor(r*7)

        new_piece%blocks = 0

        select case (rint)
            case (0)
                new_piece%blocks(1:2,1:2) = BLUE
            case (1)
                new_piece%blocks(:,2) = LIGHTGREEN
            case (2)
                new_piece%blocks(2,1:3) = RED
                new_piece%blocks(1,2) = RED
            case (3)
                new_piece%blocks(1,2:3) = MAGENTA
                new_piece%blocks(2,1:2) = MAGENTA
            case (4)
                new_piece%blocks(2,2:3) = CYAN
                new_piece%blocks(1,1:2) = CYAN
            case (5)
                new_piece%blocks(0:2,1) = GREEN
                new_piece%blocks(2,2) = GREEN
            case (6)
                new_piece%blocks(0:2,2) = LIGHTRED
                new_piece%blocks(2,1) = LIGHTRED
        end select
    
    end function

end module m_piece